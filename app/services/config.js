// @flow

import buildConfig from 'joi-config-generator';
import Joi from 'joi';

const config = {};

const schema = Joi.object().keys({
  NODE_ENV: Joi.string().valid(['production', 'development']).description('Is this a production or development environment?'),
  api: Joi.object().keys({
    port: Joi.number().port().description('Port to use for the HTTP server'),
  }),
  hashRounds: Joi.number()
    .integer()
    .default(8)
    .description('Please read the doc on rounds: https://www.npmjs.com/package/bcrypt'),
  database: Joi.object().keys({
    uri: Joi.string()
      .trim()
      .description('eg: postgres://ephys:password@localhost:5432/dbname'),
  }),
});

const useEnv = process.env.USE_ENV === 'true';

export const configReady = buildConfig({
  schema,

  allowFile: !useEnv,
  allowPrompt: !useEnv,
  allowEnv: useEnv,

  file: {
    format: 'env',
    path: `${__dirname}/../../.env`,
  },
});

configReady.then(generatedConfig => {
  Object.assign(config, generatedConfig);
});

export default config;
