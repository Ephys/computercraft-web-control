import Sequelize from 'sequelize';

export type SequelizeRelationParam = {
  transaction?: ?Sequelize.Transaction,
};
