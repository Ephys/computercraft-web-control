// @flow

import { Model, DataTypes } from 'sequelize';
import { Options, Attributes, BelongsTo } from '@stilt/sequelize';
import ActionType from './ActionType';

@Options({
  tableName: 'mc_registered_actions',
  freezeTableName: true,
  underscored: true,
})
@Attributes({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  uuid: {
    type: DataTypes.UUID,
    allowNull: false,
    unique: true,
    defaultValue: DataTypes.UUIDV4,
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE,
  },
})
@BelongsTo(ActionType, {
  as: 'actionType',
  foreignKey: {
    field: 'action_type_id',
    allowNull: false,
  },
})
export default class ExecutedAction extends Model {

  id: number;
  uuid: string;

  get executedAt() {
    return this.createdAt;
  }

  set executedAt(val) {
    this.createdAt = val;
  }
}
