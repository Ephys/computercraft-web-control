// @flow

import { Model, DataTypes } from 'sequelize';
import { Options, Attributes } from '@stilt/sequelize';

@Options({
  tableName: 'mc_action_types',
  freezeTableName: true,
  underscored: true,
})
@Attributes({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  uuid: {
    type: DataTypes.UUID,
    allowNull: false,
    unique: true,
    defaultValue: DataTypes.UUIDV4,
  },
  name: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: 'workspace-actiontype',
  },
  workspace: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: 'workspace-actiontype',
  },
  title: {
    type: DataTypes.STRING(50),
    allowNull: false,
  },
})
export default class ActionType extends Model {

  id: number;
  uuid: string;
  name: string;
  workspace: string;
}
