// @flow

import { resolve } from '@stilt/graphql';
import ActionType from '../models/ActionType';
import ExecutedAction from '../models/ExecutedAction';

export default class ActionController {

  @resolve('Query.actionTypes')
  static getActionTypes({ workspace }: { workspace: string }): Promise<ActionType[]> {
    return ActionType.findAll({
      where: {
        workspace,
      },
    });
  }

  @resolve('Query.executedActions')
  static getExecutedActions({ workspace }: { workspace: string }): Promise<ExecutedAction[]> {
    return ExecutedAction.findAll({
      include: [{
        model: ActionType,
        as: 'actionType',
        required: true,
        where: { workspace },
      }],
    });
  }

  @resolve('ExecutedAction.type')
  static getExecutedActionType({ parent }): Promise<ActionType> {
    return parent.getActionType();
  }

  @resolve('Mutation.executeAction')
  static async executeAction({ workspace, actionType: name }): Promise<ExecutedAction> {

    const actionType = await ActionType.findOne({
      where: {
        workspace,
        name,
      },
    });

    if (actionType == null) {
      throw new Error('Invalid ActionType');
    }

    return ExecutedAction.create({
      action_type_id: actionType.id,
    });
  }

  @resolve('Mutation.registerActionType')
  static async registerActionType(data): Promise<ActionType> {

    const [actionType] = await ActionType.upsert(data, {
      where: data,
      returning: true,
    });

    return actionType;
  }
}
