// @flow

import { resolve } from '@stilt/graphql';

export default class HelloController {

  @resolve('Query.status')
  static helloRoute() {
    return 'Online';
  }
}
