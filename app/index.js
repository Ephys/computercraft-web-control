// @flow

import App from '@stilt/core';
import StiltHttp from '@stilt/http';
import StiltGraphql from '@stilt/graphql';
import StiltSequelize from '@stilt/sequelize';
import cors from '@koa/cors';
import { configReady } from './services/config';

configReady.then(config => {

  const app = new App();

  const stiltHttp = new StiltHttp({
    port: config.api.port,
  });

  // TODO move CORS to Stilt
  stiltHttp.koa.use(cors());

  app.use(stiltHttp);

  app.use(new StiltGraphql({
    resolvers: `${__dirname}/controllers`,
    schema: `${__dirname}/schema`,
  }));

  const stiltSequelize = new StiltSequelize({
    models: `${__dirname}/models`,
    databaseUri: config.database.uri,
  });

  setTimeout(() => {
    // TODO CLI tool to reset DB

    stiltSequelize.sequelize.sync({ force: true });
  }, 1000);

  app.use(stiltSequelize);
});
